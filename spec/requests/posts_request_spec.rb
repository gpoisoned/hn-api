require 'rails_helper'

RSpec.describe 'Posts', type: :request do

  def mock_service_call(method, method_args, status, response)
    mock = double(HackerNewsPosts::Service)
    allow(HackerNewsPosts::Service).to receive(:new).and_return(mock)
    expect(mock).to receive(method.to_sym).with(method_args).and_return(
      {
        status: status,
        response: response
      }
    )
  end

  describe 'GET /show' do
    it 'returns http success' do
      mock_service_call(:post, { id: 1 }, 200, {})

      get '/posts/1'

      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET /index' do
    it 'returns http success' do
      mock_service_call(:posts, { ids: 1..10, request_params: anything }, 200, {})

      get '/posts'
      expect(response).to have_http_status(:success)
    end
  end
end
