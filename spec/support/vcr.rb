VCR.configure do |config|
  config.cassette_library_dir = 'spec/fixtures/cassettes'
  config.allow_http_connections_when_no_cassette = false
  config.default_cassette_options = { :record => :once }
  config.hook_into :faraday
end
