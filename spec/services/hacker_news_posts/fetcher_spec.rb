require 'rails_helper'

RSpec.describe HackerNewsPosts::Fetcher do
  let(:ids) { [1, 2] }
  let(:mock_api) { double(HackerNewsClient::Api) }
  let(:mock_response) do
    HackerNewsClient::Response.new(
      status: 200,
      body: { foo: 'bar' }
    )
  end

  before(:each) do
    allow(HackerNewsClient::Api).to receive(:new).and_return(mock_api)
  end

  it 'should call fetch items' do
    expect(mock_api).to receive(:fetch_items).with(items: ids).and_return(mock_response)

    subject.fetch_posts(ids: ids)
  end

  context 'Search Result caching' do
    before(:each) do
      allow(mock_api).to receive(:fetch_items).and_return(mock_response)
    end

    context 'With no cached entries' do
      it 'should cache the search' do
        expect {
          subject.fetch_posts(ids: [99, 10])
        }.to change { Search.count }.by(1)
      end
    end

    context 'With cached entries' do
      it 'should increment hits' do
        search = create(:search, start_id: 99, end_id: 99, hits: 0)
        create(:search_result, search: search)

        subject.fetch_posts(ids: [99])
        search.reload
        expect(search.hits).to eq(1)

        subject.fetch_posts(ids: [99])
        search.reload
        expect(search.hits).to eq(2)
      end
    end

    it 'should use cached value if the requested range is within cached range' do
      search = create(:search, start_id: 9, end_id: 99, hits: 0)
      create(:search_result, search: search)

      subject.fetch_posts(ids: 40..50)
      search.reload
      expect(search.hits).to eq(1)
    end

    it 'should filter the result if the request range is within cached range' do
      search = create(:search, start_id: 1, end_id: 5, hits: 0)
      create(:search_result, search: search, body: [
        {
          id: 1,
          text: 'a'
        },
        {
          id: 2,
          text: 'b'
        },
        {
          id: 3,
          text: 'c'
        },
        {
          id: 4,
          text: 'd'
        },
        {
          id: 5,
          text: 'e'
        }
      ].to_json)

      result = subject.fetch_posts(ids: 2..3)
      result_ids = result[:body].map { |r| r[:id] }
      expect(result_ids).to match_array([2, 3])
    end

    it 'should not use cached value if the requested range is outside of the cached range' do
      search = create(:search, start_id: 9, end_id: 99, hits: 0)
      create(:search_result, search: search)

      subject.fetch_posts(ids: 8..50)
      search.reload
      expect(search.hits).to eq(0)
    end

    it 'should handle null values' do
      search = create(:search, start_id: 9, end_id: 9, hits: 0)
      create(:search_result, search: search, body: "null")

      subject.fetch_posts(ids: [9])
      search.reload
      expect(search.hits).to eq(1)
    end
  end
end