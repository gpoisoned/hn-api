require 'rails_helper'

RSpec.describe HackerNewsPosts::Sort do
  let(:posts) do
    [
      {
        id: 1,
        score: 1.1
      },
      {
        id: 2,
        score: nil
      },
      {
        id: 3,
        score: 99
      }
    ]
  end

  context 'sort' do
    it 'does nothing when sort_by does not exist' do
      filterd = subject.sort(posts: posts, sort_key: nil, sort_direction: 'asc')

      expect(filterd[0][:id]).to eq(1)
      expect(filterd[1][:id]).to eq(2)
      expect(filterd[2][:id]).to eq(3)
    end

    it 'does nothing when sort_direction does not exist' do
      filterd = subject.sort(posts: posts, sort_key: 'score', sort_direction: nil)

      expect(filterd[0][:id]).to eq(1)
      expect(filterd[1][:id]).to eq(2)
      expect(filterd[2][:id]).to eq(3)
    end

    it "sorts in 'asc' direction correctly" do
      filterd = subject.sort(posts: posts, sort_key: 'score', sort_direction: 'asc')

      expect(filterd[0][:id]).to eq(2)
      expect(filterd[1][:id]).to eq(1)
      expect(filterd[2][:id]).to eq(3)
    end

    it "sorts in 'desc' direction correctly" do
      filterd = subject.sort(posts: posts, sort_key: 'score', sort_direction: 'desc')

      expect(filterd[0][:id]).to eq(3)
      expect(filterd[1][:id]).to eq(1)
      expect(filterd[2][:id]).to eq(2)
    end
  end
end
