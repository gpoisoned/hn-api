require 'rails_helper'

RSpec.describe HackerNewsPosts::Filter do
  let(:posts) do
    [
      {
        id: 1,
        type: 'story',
        by: 'a'
      },
      {
        id: 2,
        type: 'comment',
        by: 'b'
      },
      {
        id: 3,
        type: 'story',
        by: 'b'
      }
    ]
  end

  context 'filter' do
    it "filters by 'type' correctly" do
      filterd = subject.filter(posts: posts, filter_key: 'type', filter_value: 'story')

      expect(filterd.size).to eq(2)
      expect(filterd.map{ |p| p[:id] }).to match_array([1, 3])
    end

    it "filters by 'by' correctly" do
      filterd = subject.filter(posts: posts, filter_key: :by, filter_value: 'b')

      expect(filterd.size).to eq(2)
      expect(filterd.map{ |p| p[:id] }).to match_array([2, 3])
    end

    it "passes through posts correctly if filter_key is not correct" do
      filterd = subject.filter(posts: posts, filter_key: :id, filter_value: 'b')

      expect(filterd.size).to eq(3)
      expect(filterd.map{ |p| p[:id] }).to match_array([1, 2, 3])
    end
  end
end
