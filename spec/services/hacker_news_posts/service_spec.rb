require 'rails_helper'

RSpec.describe HackerNewsPosts::Service do
  let(:ids) { 1..10 }
  let(:mock_fetcher) { double(HackerNewsPosts::Fetcher) }
  let(:mock_posts) do
    [
      {
        id: 1,
        text: 'hello'
      }
    ]
  end
  let(:mock_response) do
    {
      status: 200,
      body: mock_posts
    }
  end

  before(:each) do
    allow(HackerNewsPosts::Fetcher).to receive(:new).and_return(mock_fetcher)
  end

  context 'posts' do
    it 'should call fetch_posts' do
      expect(mock_fetcher).to receive(:fetch_posts).with(ids: ids).and_return(
        {
          status: 200,
          body: "hello"
        }
      )

      result = subject.posts(ids: ids, request_params: RequestParams.new)
      expect(result[:status]).to eq(200)
      expect(result[:response]).to eq(
        {
          data: "hello"
        }
      )
    end

    it 'filters the data if filter params is present' do
      request_params = RequestParams.new
      request_params.type = 'story'

      mock_filter_service = double(HackerNewsPosts::Filter)
      allow(HackerNewsPosts::Filter).to receive(:new).and_return(mock_filter_service)

      expect(mock_filter_service).to receive(:filter).with(
        posts: mock_posts,
        filter_key: 'type',
        filter_value: 'story'
      )
      expect(mock_fetcher).to receive(:fetch_posts).with(
        ids: 1..10
      ).and_return(mock_response)

      subject.posts(ids: 1..10, request_params: request_params)
    end

    it 'sorts the data if sort params is present' do
      request_params = RequestParams.new
      request_params.sort_by = 'score'
      request_params.sort_order = 'desc'

      mock_sort_service = double(HackerNewsPosts::Sort)
      allow(HackerNewsPosts::Sort).to receive(:new).and_return(mock_sort_service)

      expect(mock_sort_service).to receive(:sort).with(
        posts: mock_posts,
        sort_key: 'score',
        sort_direction: 'desc'
      )
      expect(mock_fetcher).to receive(:fetch_posts).with(
        ids: 1..10
      ).and_return(mock_response)

      subject.posts(ids: 1..10, request_params: request_params)
    end
  end

  context 'error' do
    it 'should throw an error if request_params is invalid' do
      invalid_request_params = RequestParams.new
      invalid_request_params.start = 100
      invalid_request_params.end = 99

      result = subject.posts(ids: ids, request_params: invalid_request_params)
      expect(result[:status]).to eq(400)
      expect(result[:response][:errors]).to match_array(["End must be greater than or equal to Start"])
    end

    it 'should have the correct status' do
      expect(mock_fetcher).to receive(:fetch_posts).with(ids: ids).and_return(
        {
          status: 500,
          body: "Service is unavailable"
        }
      )

      result = subject.posts(ids: ids, request_params: RequestParams.new)
      expect(result[:status]).to eq(500)
      expect(result[:response]).to eq(
        {
          data: "Service is unavailable"
        }
      )
    end
  end
end
