# == Schema Information
#
# Table name: searches
#
#  id         :bigint           not null, primary key
#  hits       :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  end_id     :bigint           not null
#  start_id   :bigint           not null
#
# Indexes
#
#  index_searches_on_start_id_and_end_id  (start_id,end_id) UNIQUE
#
FactoryBot.define do
  factory :search do
    start_id { 1 }
    end_id { 100 }
    hits { 1 }
  end
end
