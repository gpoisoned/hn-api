# == Schema Information
#
# Table name: search_results
#
#  id         :bigint           not null, primary key
#  body       :jsonb
#  status     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  search_id  :bigint           not null
#
# Indexes
#
#  index_search_results_on_search_id  (search_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (search_id => searches.id) ON DELETE => cascade
#
FactoryBot.define do
  factory :search_result do
    body do
      [
        {
          "id": 1,
          "by": "pg",
          "score": 57,
          "time": 1160418111,
          "title": "Y Combinator",
          "type": "story",
          "text": nil,
          "url": "http://ycombinator.com",
          "parent": nil,
          "kids": [
            15,
            234509,
            487171,
            454426,
            454424,
            454410,
            82729
          ]
        }
      ].to_json
    end
    status { 200 }

    search
  end
end
