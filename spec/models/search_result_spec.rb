# == Schema Information
#
# Table name: search_results
#
#  id         :bigint           not null, primary key
#  body       :jsonb
#  status     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  search_id  :bigint           not null
#
# Indexes
#
#  index_search_results_on_search_id  (search_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (search_id => searches.id) ON DELETE => cascade
#
require 'rails_helper'

RSpec.describe SearchResult, type: :model do
  let(:subject) { create(:search_result) }

  it 'should have a valid factory' do
    expect(subject.valid?).to eq(true)
  end

  it { should belong_to(:search) }
end
