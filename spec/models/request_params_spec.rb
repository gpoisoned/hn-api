require 'rails_helper'

RSpec.describe RequestParams, type: :model do
  context 'from_controller_params' do
    it 'should create a new object' do
      result = described_class.from_controller_params(
        {
          by: 'pg',
          type: 'story'
        },
        {
          sort_by: 'time',
          sort_order: 'desc'
        }
      )

      expect(result).to be_a(RequestParams)
      expect(result.by).to eq('pg')
      expect(result.type).to eq('story')
      expect(result.sort_by).to eq('time')
      expect(result.sort_order).to eq('desc')
    end

    it 'should default start and end' do
      result = described_class.from_controller_params

      expect(result).to be_a(RequestParams)
      expect(result.start).to eq(1)
      expect(result.end).to eq(10)
    end

    it 'should convert start and end to an integer' do
      result = described_class.from_controller_params(
        {
          start: '1',
          end: '2'
        }
      )

      expect(result).to be_a(RequestParams)
      expect(result.start).to eq(1)
      expect(result.end).to eq(2)
    end
  end

  context 'filter_params' do
    it 'returns all filter params' do
      subject.start = 1
      subject.end = 50
      subject.by = 'pg'
      subject.type = 'story'
      subject.sort_order = 'desc'
      subject.sort_by = 'time'

      expected = [
        {
          filter_value: 1,
          filter_key: 'start'
        },
        {
          filter_value: 50,
          filter_key: 'end'
        },
        {
          filter_value: 'pg',
          filter_key: 'by'
        },
        {
          filter_value: 'story',
          filter_key: 'type'
        }
      ]
      expect(subject.filter_params).to match_array(expected)
    end
  end

  context 'sort_params' do
    it 'returns all sort params' do
      subject.start = 1
      subject.end = 50
      subject.by = 'pg'
      subject.type = 'story'
      subject.sort_order = 'desc'
      subject.sort_by = 'time'

      expected = [
        {
          sort_key: 'time',
          sort_direction: 'desc'
        }
      ]
      expect(subject.sort_params).to match_array(expected)
    end
  end


  context 'validations' do
    it 'should be valid on initialization' do
      expect(subject.valid?).to eq(true)
    end

    context 'range' do
      it 'should be valid if range is less than MAX_ITEMS_PER_REQUEST' do
        subject.start = 1
        subject.end = 50

        expect(subject.valid?).to eq(true)
      end

      it 'should be valid if range is equal to the MAX_ITEMS_PER_REQUEST' do
        subject.start = 1
        subject.end = 201

        expect(subject.valid?).to eq(true)
      end

      it 'should be valid if range is equal to the MAX_ITEMS_PER_REQUEST' do
        subject.start = 1
        subject.end = 202

        expect(subject.valid?).to eq(false)
      end
    end

    context 'by' do
      it "should be valid if 'by' is present" do
        subject.by = 'a'

        expect(subject.valid?).to eq(true)
      end
    end

    context 'start and end' do
      it "should be invalid if only 'end' is present" do
        subject.end = 1

        expect(subject.valid?).to eq(false)
      end

      it "should be invalid if only 'start' is present" do
        subject.start = 1

        expect(subject.valid?).to eq(false)
      end

      it "should be invalid if 'start' is negative" do
        subject.start = -1

        expect(subject.valid?).to eq(false)
      end

      it "should be invalid if 'start' is a string" do
        subject.start = '1'

        expect(subject.valid?).to eq(false)
      end

      it "should be invalid if 'end' is a string" do
        subject.end = '1'

        expect(subject.valid?).to eq(false)
      end

      it "should be invalid if 'start' is greater than 'end'" do
        subject.start = 100
        subject.end = 10

        expect(subject.valid?).to eq(false)
      end

      it "should be valid if 'start' is equal than 'end'" do
        subject.start = 100
        subject.end = 100

        expect(subject.valid?).to eq(true)
      end

      it "should be valid if 'start' is equal than 'end'" do
        subject.start = 100
        subject.end = 100

        expect(subject.valid?).to eq(true)
      end
    end

    context 'type' do
      %w(comment story job poll pollopt).each do |type|
        it "should be valid if type is '#{type}'" do
          subject.type = type

          expect(subject.valid?).to eq(true)
        end
      end

      %w(:comment anything).each do |type|
        it "should be invalid if type is '#{type}'" do
          subject.type = type

          expect(subject.valid?).to eq(false)
        end
      end
    end

    context 'sort_by' do
      %w(score time).each do |type|
        it "should be valid if sort_by is '#{type}'" do
          subject.sort_by = type

          expect(subject.valid?).to eq(true)
        end
      end

      %w(id text).each do |type|
        it "should be invalid if sort_by is '#{type}'" do
          subject.sort_by = type

          expect(subject.valid?).to eq(false)
        end
      end
    end

    context 'sort_order' do
      %w(asc desc).each do |type|
        it "should be valid if sort_order is '#{type}'" do
          subject.sort_order = type

          expect(subject.valid?).to eq(true)
        end
      end

      %w(ascending descending).each do |type|
        it "should be invalid if sort_order is '#{type}'" do
          subject.sort_order = type

          expect(subject.valid?).to eq(false)
        end
      end
    end
  end
end
