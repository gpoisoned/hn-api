# == Schema Information
#
# Table name: searches
#
#  id         :bigint           not null, primary key
#  hits       :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  end_id     :bigint           not null
#  start_id   :bigint           not null
#
# Indexes
#
#  index_searches_on_start_id_and_end_id  (start_id,end_id) UNIQUE
#
require 'rails_helper'

RSpec.describe Search, type: :model do
  let(:subject) { create(:search) }

  it 'should have a valid factory' do
    expect(subject.valid?).to eq(true)
  end

  context 'validations' do
    it { should validate_presence_of(:start_id) }
    it { should validate_presence_of(:end_id) }
    it { should validate_uniqueness_of(:start_id).scoped_to(:end_id) }

    it { should have_one(:search_result) }
  end
end
