require 'rails_helper'

  module HackerNewsClient
   RSpec.describe Api do
     let(:test_id) { 1 }
     let(:non_existen_id) { -1 }

      context 'fetch_item' do
       it 'returns a correct response for a valid id' do
         VCR.use_cassette("item/#{test_id}") do
           response = subject.fetch_item(item_id: test_id)

            expect(response).to be_a(HackerNewsClient::Response)
           expect(response.status).to eq(200)
           expect(response.body).to eq({
             :id => 1,
             :by => "pg",
             :score => 57,
             :time => 1160418111,
             :title => "Y Combinator",
             :type => "story",
             :text => nil,
             :url => "http://ycombinator.com",
             :parent => nil,
             :kids => [
               15,
               234509,
               487171,
               454426,
               454424,
               454410,
               82729
             ]
           })
         end
       end

        it 'returns a correct response for a non-existent id' do
         VCR.use_cassette("item/#{non_existen_id}") do
           response = subject.fetch_item(item_id: non_existen_id)

            expect(response).to be_a(HackerNewsClient::Response)
           expect(response.status).to eq(200)
           expect(response.body).to eq(nil)
         end
       end
     end

      context 'fetch_items' do
       let(:item_ids) { 1..10 }

        it 'returns a correct response for a valid id' do
         VCR.use_cassette("fetch_items", record: :new_episodes) do
           response = subject.fetch_items(items: item_ids)

            expect(response).to be_a(HackerNewsClient::Response)
           expect(response.status).to eq(200)

            response_ids = response.body.map { |r| r[:id] }
           expect(response_ids).to match_array(item_ids)
         end
       end
     end
   end
 end
