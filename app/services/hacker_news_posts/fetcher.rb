module HackerNewsPosts
  class Fetcher
    def initialize
      @api_client = HackerNewsClient::Api.new
    end

    def fetch_posts(ids:)
      get_result(ids)
    end

    private

    def get_result(ids)
      start_id = ids&.min
      end_id = ids&.max

      search = Search.where('start_id <= ? AND end_id >= ?', start_id, end_id).first
      if search
        search_result = search.search_result
        search.update(hits: search.hits + 1)

        parsed_json = JSON.parse(search_result.body)
        if parsed_json && parsed_json.is_a?(Array)
          parsed_json = parsed_json.compact.map(&:with_indifferent_access)
          parsed_json = parsed_json.select { |post| ids.include?(post[:id]) }
        end

        {
          status: search_result.status,
          meta: {
            cached: true,
            hits: search.hits
          },
          body: parsed_json
        }
      else
        response = @api_client.fetch_items(items: ids.to_a)

        cache_search(ids: ids, response: response)
        {
          status: response.status,
          meta: {
            cached: false,
            hits: 1
          },
          body: response.body
        }
      end
    end

    def cache_search(ids:, response:)
      start_id = ids&.min
      end_id = ids&.max

      ActiveRecord::Base.transaction do
        search_id = upsert_search(start_id, end_id)
        upsert_search_result(search_id, response)
      end
    end

    def upsert_search(start_id, end_id)
      query = <<~SQL
        INSERT INTO searches (start_id, end_id, hits, created_at, updated_at)
        VALUES (#{start_id}, #{end_id}, 1, NOW(), NOW())
        ON CONFLICT (start_id, end_id)
        DO
          UPDATE SET hits = searches.hits + 1,
                     updated_at = NOW()
        RETURNING searches.id
      SQL

      result = ActiveRecord::Base.connection.execute(query)
      result.first['id']
    end

    def upsert_search_result(search_id, response)
      json_body = JSON.generate(response.body)

      attributes = {
        body: json_body,
        status: response.status,
        search_id: search_id,
        created_at: Time.now,
        updated_at: Time.now
      }

      SearchResult.upsert(attributes, unique_by: :search_id)
    end
  end
end
