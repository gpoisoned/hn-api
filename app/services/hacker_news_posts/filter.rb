module HackerNewsPosts
  class Filter
    def filter(posts:, filter_key:, filter_value:)
      case filter_key&.to_sym
      when :type
        filtered_results(posts, :type, filter_value)
      when :by
        filtered_results(posts, :by, filter_value)
      else
        posts
      end
    end

    private

    def filtered_results(posts, key, value)
      return posts unless value

      posts.select { |p| p[key] == value }
    end
  end
end
