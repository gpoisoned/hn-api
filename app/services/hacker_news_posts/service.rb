module HackerNewsPosts
  class Service
    attr_reader :fetcher

    def initialize
      @fetcher = Fetcher.new
    end

    def post(id:)
      api_response = fetcher.fetch_posts(ids: [id])
      status = api_response[:status]
      body = api_response[:body]&.first
      meta = api_response[:meta]

      result(status, body, [], meta)
    end

    def posts(ids:, request_params:)
      if request_params.valid?
        api_response = fetcher.fetch_posts(ids: ids)
        status = api_response[:status]
        body = api_response[:body]
        meta = api_response[:meta]
        posts = posts_response(body, request_params)

        result(status, posts, [], meta)
      else
        result(400, nil, request_params.errors.full_messages)
      end
    end

    private

    def posts_response(posts, reqest_params)
      posts = filtered_posts(posts, reqest_params)
      posts = sorted_posts(posts, reqest_params)
      posts
    end

    def filtered_posts(posts, request_params)
      filter_service = HackerNewsPosts::Filter.new
      request_params.filter_params.each do |filter_param|
        next unless filter_param[:filter_key] && filter_param[:filter_value]

        posts = filter_service.filter(
          posts: posts,
          filter_key: filter_param[:filter_key],
          filter_value: filter_param[:filter_value]
        )
      end
      posts
    end

    def sorted_posts(posts, request_params)
      sort_service = HackerNewsPosts::Sort.new
      request_params.sort_params.each do |sort_param|
        next unless sort_param[:sort_key] && sort_param[:sort_direction]

        posts = sort_service.sort(
          posts: posts,
          sort_key: sort_param[:sort_key],
          sort_direction: sort_param[:sort_direction]
        )
      end
      posts
    end

    def result(status, data, errors = [], meta = {})
      result = base_result
      result[:status] = status
      result[:response][:data] = data
      result[:response][:meta] = meta if meta.present?
      result[:response][:errors] = errors if errors.present?
      result
    end

    def base_result
      {
        status: nil,
        response: {
          data: nil
        }
      }
    end
  end
end
