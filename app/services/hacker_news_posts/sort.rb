module HackerNewsPosts
  class Sort
    def sort(posts:, sort_key:, sort_direction:)
      return posts unless sort_key && sort_direction

      posts.sort_by do |post|
        post = post.with_indifferent_access

        sort_score = post[sort_key.to_sym] || 0
        if sort_direction.to_sym == :desc
          sort_score * -1
        else
          sort_score
        end
      end
    end
  end
end
