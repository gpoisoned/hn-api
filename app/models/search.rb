# == Schema Information
#
# Table name: searches
#
#  id         :bigint           not null, primary key
#  hits       :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  end_id     :bigint           not null
#  start_id   :bigint           not null
#
# Indexes
#
#  index_searches_on_start_id_and_end_id  (start_id,end_id) UNIQUE
#
class Search < ApplicationRecord
  validates :start_id, presence: true
  validates :end_id, presence: true

  validates_uniqueness_of :start_id, scope: :end_id

  has_one :search_result
end
