class RequestParams
  include ActiveModel::Validations

  def self.from_controller_params(filter_params = {}, sort_params = {})
    request_params = RequestParams.new

    # Add query range
    request_params.start = filter_params[:start]&.to_i || 1
    request_params.end = filter_params[:end]&.to_i || 10

    # Add filter params
    request_params.type = filter_params[:type] if filter_params.key?(:type)
    request_params.by = filter_params[:by] if filter_params.key?(:by)

    # Add sort params
    request_params.sort_by = sort_params[:sort_by] if sort_params.key?(:sort_by)
    request_params.sort_order = sort_params[:sort_order] if sort_params.key?(:sort_order)

    request_params
  end

  FILTER_KEYS = %w[start end by type].freeze
  SORT_KEYS = %w[sort_by sort_order].freeze

  MAX_ID = 99999999
  MAX_ITEMS_PER_REQUEST = 200

  # Custom validation error messages
  INVALID_TYPE = "%{value} is not a valid type".freeze
  INVALID_SORT_BY = "%{value} is not a valid sort_by".freeze
  INVALID_SORT_ORDER = "%{value} is not a valid sort_order".freeze
  INVALID_TO = "must be greater than or equal to Start".freeze

  validates :by, format: { with: /[a-zA-Z0-9]*/ }, if: proc { |r| r.by }
  validates :type, inclusion: { in: %w(comment story job poll pollopt), message: INVALID_TYPE }, if: proc { |r| r.type }

  # Both start and end must be present if either of them is present
  validates :start, presence: true, if: proc { |r| r.end }
  validates :end, presence: true, if: proc { |r| r.start }

  # Start must be a positive integer
  validates :start, numericality: { only_integer: true, greater_than: 0, allow_nil: true }, if: proc { |r| r.start && r.end }

  # End must be greater than or equal to start if both start and end is present
  validates :end, numericality: { only_integer: true, greater_than_or_equal_to: :start, allow_nil: true, message: INVALID_TO }, if: proc { |r| r.start && r.end }

  # End must be smaller than the MAX_ID if end is present
  validates :end, numericality: { only_integer: true, less_than: MAX_ID, allow_nil: true }, if: proc { |r| r.end }

  # Start and end must be within the maxium items per request
  validate :validate_item_range, if: proc { |r| r.start && r .end }

  validates :sort_by, inclusion: { in: %w(score time), message: INVALID_SORT_BY }, if: proc { |r| r.sort_by }
  validates :sort_order, inclusion: { in: %w(asc desc), message: INVALID_SORT_ORDER }, if: proc { |r| r.sort_order }

  attr_accessor :start, :end, :by, :type, :sort_by, :sort_order

  def filter_params
    FILTER_KEYS.map do |key|
      {
        filter_key: key,
        filter_value: self.instance_variable_get("@#{key}")
      }
    end
  end

  def sort_params
    [
      {
        sort_key: self.instance_variable_get("@sort_by"),
        sort_direction: self.instance_variable_get("@sort_order")
      }
    ]
  end

  private

  def validate_item_range
    delta = self.end.to_i - self.start.to_i
    if delta > MAX_ITEMS_PER_REQUEST
      errors.add(:range, "must not be greater than #{MAX_ITEMS_PER_REQUEST}")
    end
  end
end