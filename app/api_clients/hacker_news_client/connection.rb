module HackerNewsClient
  class Connection
    attr_reader :base_url, :fd_connection

    def initialize(base_url:)
      @base_url = base_url
      @fd_connection ||= Faraday.new(
        url: @base_url,
        headers: { 'Content-Type' => 'application/json' }
      )
   end

    def get(path:, &block)
      get_url = base_url + path + '.json'
      res = fd_connection.get(get_url)
      body = if block_given?
               yield res.body
             else
               res.body
             end

      Response.new(status: res.status, body: body)
   end
  end
end
