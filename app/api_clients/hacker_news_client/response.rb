module HackerNewsClient
  Response = Struct.new(
    :status,
    :body,
    keyword_init: true
  )
end
