module HackerNewsClient
  class Api
    MAX_THREADS = 4
    API_ENDPOINT_BASE_URL = 'https://hacker-news.firebaseio.com/v0/'.freeze
    VALID_KEYS = %i[id by score time title type text url parent kids].freeze

    attr_reader :connection

    def initialize
      @connection = Connection.new(base_url: API_ENDPOINT_BASE_URL)
    end

    def fetch_item(item_id:)
      return invalid_input_response unless item_id

      connection.get(path: "/item/#{item_id}") do |body|
        return {} unless body

        response_body(body)
      end
    end

    def fetch_items(items: [])
      start_time = Time.now.to_i
      result = concurrent_fetch_items(items)
               .filter { |res| res.status = 200 }
               .map(&:body)
      end_time = Time.now.to_i
      Rails.logger.info "CONCURRENT_FETCH_ITEMS: total time = #{end_time - start_time}"

      Response.new(status: 200, body: result)
    end

    private

    def concurrent_fetch_items(items)
      result = []
      mutex = Mutex.new

      work_queue = Queue.new
      items.each { |item| work_queue << item }
      workers = (0..MAX_THREADS).map do
        Thread.new do
          until work_queue.empty?
            id = work_queue.pop(true)

            res = fetch_item(item_id: id)
            mutex.synchronize {
              result << res
            }
          end
        end
      end

      workers.map(&:join)
      result
    end

    def invalid_input_response
      Response.new(status: 422, body: {})
    end

    def response_body(body)
      parsed = JSON.parse(body)
      return nil if parsed.nil?

      if parsed.is_a?(Array)
        parsed.each { |item| response_hash(item) }
      else
        response_hash(parsed)
      end
    rescue JSON::ParserError
      nil
    end

    def response_hash(hash)
      VALID_KEYS.each_with_object({}) do |key, acc|
        acc[key] = hash[key.to_s]
      end
    end
  end
end
