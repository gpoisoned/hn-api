class PostsController < ApplicationController
  def show
    res = hn_posts_service.post(id: params[:id].to_i)

    render json: res[:response], status: res[:status]
  end

  def index
    range = Range.new(request_params.start, request_params.end)
    res = hn_posts_service.posts(ids: range, request_params: request_params)

    render json: res[:response], status: res[:status]
  end

  private

  def hn_posts_service
    @hacker_news_service ||= HackerNewsPosts::Service.new
  end

  def request_params
    @request_params ||= RequestParams.from_controller_params(filter_params, sort_params)
  end

  def filter_params
    params.permit(:type, :start, :end, :by)
  end

  def sort_params
    params.permit(:sort_by, :sort_order)
  end
end
