# README

## Setup Database

```ruby
bundle exec rails db:create && bundle exec rails db:migrate
```

## How to run the test suite

    bundle exec rails db:test:prepare
    bundle exec rspec

## Deployed App URL

https://powerful-plateau-13096.herokuapp.com/

## Endpoints

### `/posts`

This endpoit returns a list of posts.

Filter Params:

| Name   | Value                                               | Description          |
| -------| ----------------------------------------------------|----------------------|
| start  | Integer (defaults to 1)                             | Id of the first post |
| end    | Integer (defaults to 10)                            | Id of the last post  |
| type   | String (story or comment or job or poll or pollopt) | Type of the post     |
| by     | String (alphanumeric value)                         | Creator of the post  |

Sort Params:

| Name       | Value                   | Description               |
| -----------| ------------------------|---------------------------|
| sort_by    | String (time or score)  | Field used to sort posts  |
| sort_order | String (asc or desc)    | The order of posts        |


### `/posts/:post_id`
This endpoint returns a single post.

## Sample Requests

1. Get post

[https://powerful-plateau-13096.herokuapp.com/posts/1](https://powerful-plateau-13096.herokuapp.com/posts/1)

```
curl -X GET https://powerful-plateau-13096.herokuapp.com/posts/1
```

2. List posts

[https://powerful-plateau-13096.herokuapp.com/posts](https://powerful-plateau-13096.herokuapp.com/posts)

```
curl -X GET https://powerful-plateau-13096.herokuapp.com/posts
```

3. List posts with filter params

[https://powerful-plateau-13096.herokuapp.com/posts?start=1&end=150&type=story&by=pg](https://powerful-plateau-13096.herokuapp.com/posts?start=1&end=150&type=story&by=pg)

```
curl -X GET https://powerful-plateau-13096.herokuapp.com/posts?start=1&end=150&type=story&by=pg
```

4. List posts with filter params and sort params

[https://powerful-plateau-13096.herokuapp.com/posts?start=1&end=150&type=story&by=pg&sort_by=score&sort_order=desc](https://powerful-plateau-13096.herokuapp.com/posts?start=1&end=150&type=story&by=pg&sort_by=score&sort_order=desc)

```
curl -X GET https://powerful-plateau-13096.herokuapp.com/posts?start=1&end=150&type=story&by=pg&sort_by=score&sort_order=desc

```

5. Bad request

[https://powerful-plateau-13096.herokuapp.com/posts?type=foobar](https://powerful-plateau-13096.herokuapp.com/posts?type=foobar)

```
curl -X GET https://powerful-plateau-13096.herokuapp.com/posts?type=foobar
```

[https://powerful-plateau-13096.herokuapp.com/posts?start=1&end=999999](https://powerful-plateau-13096.herokuapp.com/posts?start=1&end=999999)

```
curl -X GET https://powerful-plateau-13096.herokuapp.com/posts?start=1&end=999999
```
