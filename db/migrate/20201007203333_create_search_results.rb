class CreateSearchResults < ActiveRecord::Migration[6.0]
  def change
    create_table :search_results do |t|
      t.references :search, index: { unique: true }, foreign_key: { on_delete: :cascade }, null: false
      t.jsonb :body
      t.integer :status

      t.timestamps
    end
  end
end
