class CreateSearches < ActiveRecord::Migration[6.0]
  def change
    create_table :searches do |t|
      t.bigint :start_id, null: false
      t.bigint :end_id, null: false
      t.bigint :hits
      t.timestamps

      t.index [:start_id, :end_id], unique: true
    end
  end
end
