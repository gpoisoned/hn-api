# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_10_07_203333) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "search_results", force: :cascade do |t|
    t.bigint "search_id", null: false
    t.jsonb "body"
    t.integer "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["search_id"], name: "index_search_results_on_search_id", unique: true
  end

  create_table "searches", force: :cascade do |t|
    t.bigint "start_id", null: false
    t.bigint "end_id", null: false
    t.bigint "hits"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["start_id", "end_id"], name: "index_searches_on_start_id_and_end_id", unique: true
  end

  add_foreign_key "search_results", "searches", on_delete: :cascade
end
